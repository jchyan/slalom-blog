#Getting Started
##Step 1: Environment setup
You will need to have Java 8 along with maven 3 installed in your environment.

You will also need MongoDB setup for persistent storage.

##Step 2: Run MongoDB
Before we start running the app, we need to start the MongoDB service. Run the following:
```
sudo service mongod start
```
You can verify the service started by checking `/var/log/mongodb/mongod.log`

Later, when you want to stop the service, simply run:
```
sudo service mongod stop
```

##Step 3: Build maven project and run
Once you have the environment set up, navigate to the root directory of the project. You should see `mvnw` which we'll use to build the project and run the app by running this in your terminal:
```
./mvnw spring-boot:run
```

##(Optional): Install node dependencies on your own
Using npm, you can manage all the front-end packages and dependencies. The front-end maven plugin for the maven project should automatically install node, npm, and the node dependencies already.

If you want to install the node dependencies without going through the maven build, run the following:
```
npm install
```

##Step 4: Open a browser
Now, open up a browser and navigate to `http://localhost:8080/` and start blogging!

#Running Tests
You can run tests for front-end code by running:
```
npm test
```
This will generate a test coverage report which can be accessed in the `coverage` directory that's generated in the project directory.

#Notes
##General
* There is no actual login authentication or user customization. There is only the option to pick a username for the session.

##User Interface
* Decided to have a navigation so a user could access different components of the platform easily
* Made Home be the list of posts. This is working off the assumption we want the posts to be public and visible to anyone who visits the app

##Database
* Decided to use MongoDB because it's document-style database is easy to setup and seems to make sense for organizing blog posts. MongoDB would also allow for easy scalability if this app were to grow more complex and bigger.

#Room For Improvement
* Need to implement User collection so that users have an internal id, username, first name, last name
* Users should be identified by an id in code rather than the username string
* Should add handling of failed server requests as well as general error handling
* Can implement a more robust login. Currently, there is no real login authentication.
* Test coverage is around 80% right now. Should get better test coverage as well as more robust test code.
* The style could use improvement. There is very minimal styling.
* This is a pretty bare bones implementation of the app requirements given. It would be good to implement more features.

#Frameworks and Packages Used
* Spring Boot
* Spring Data MongoDB
* React
* React-Redux
* React-Router
* Jest
* Enzyme