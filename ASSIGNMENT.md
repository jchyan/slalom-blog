#Blog Application Overview

You are to create a POC blogging platform. The application will consist of two distinct components (evaluated independently)

* A back-end RESTful web service
* A front-end application to consume the RESTful service.

##Application Requirements

* Your blog platform must allow a user to:
    * Create new blog posts
    * View a list of all available blog posts
    * View an individual blog entry
    * Search for blog entries by title and/or by user
    * Edit their own existing blog posts
    * Delete their own existing blog posts
* The application must provide a means for a user to identify themselves (a full-fledged authentication/authorization system is not the focus of this exercise, so a formal "login" is not required)
* Each blog entry must contain the following information:
    * A unique id
    * A title
    * Blog content
    * Date posted
    * Date updated
    * Blogger info (username, first name and last name)
* You are also free to add any additional features that you deem appropriate, provided they do not conflict with the above requirements.

##Architectural Requirements

* The back-end component must be implemented using [Spring Boot](http://spring.io/) (using Java 8).
* You are welcome to use the database engine of your choice (SQL or NoSQL).
* Please include in your submission all information and files necessary to initialize your database (if any are required).
* The front-end component must be implemented using React.js and Redux.
    * [ReactJS](https://facebook.github.io/react/)
    * [Redux](http://redux.js.org/)
* The quality of the UI design is not the focus of this excercise, but you are encouraged to leverage CSS best practices.
* Your application must include adequate test coverage within both the front-end and back-end components.

As a last step, please be sure to update this README file with any instructions/documentation for executing your application. You are highly encouraged to also include additional details and insights into some of the design and architectural decisions you made within the project.

Once finished, please submit your work as a pull request against this repository.

Please feel free to submit any questions/clarifications to your Slalom contact.

Thanks!