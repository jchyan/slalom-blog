package com.example.blog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jeffrey Chyan on 6/1/17.
 */

@RestController
public class PostController {
    @Autowired
    PostRepository postRepository;

    @RequestMapping(value="/api/posts/get", method= RequestMethod.GET)
    @ResponseBody
    public HttpEntity<List<Post>> getPosts(@RequestParam(value = "title", required = false) String title,
                                           @RequestParam(value = "user", required = false) String user) {
        if (title != null && user != null) {
            return new ResponseEntity<List<Post>>(postRepository.findAllByTitleLikeIgnoreCaseOrUser(title, user), HttpStatus.OK);
        }
        else if (title != null) {
            return new ResponseEntity<List<Post>>(postRepository.findAllByTitleLikeIgnoreCase(title), HttpStatus.OK);
        }
        else if (user != null) {
            return new ResponseEntity<List<Post>>(postRepository.findAllByUser(user), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<Post>>(postRepository.findAll(), HttpStatus.OK);
        }
    }
}
