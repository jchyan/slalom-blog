package com.example.blog;

import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * Created by Jeffrey Chyan on 5/30/17.
 */

@Data
public class Post {
    @Id
    public String id;

    public String title;
    public String content;
    public String user;
    public Long datePosted;
    public Long dateUpdated;

    public Post() {}

    public Post(String title, String content, String user, Long datePosted, Long dateUpdated) {
        this.title = title;
        this.content = content;
        this.user = user;
        this.datePosted = datePosted;
        this.dateUpdated = dateUpdated;
    }
}
