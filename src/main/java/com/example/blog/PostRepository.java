package com.example.blog;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Jeffrey Chyan on 5/30/17.
 */
public interface PostRepository extends MongoRepository<Post, String> {
    public List<Post> findAllByTitleLikeIgnoreCase(String title);

    public List<Post> findAllByUser(String user);

    public List<Post> findAllByTitleLikeIgnoreCaseOrUser(String title, String user);
}
