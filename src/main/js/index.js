import React from 'react';
import ReactDOM from 'react-dom';
import reducer from './reducers';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { Router, Route, browserHistory } from 'react-router';
import App from './components/App';
import Login from './containers/Login';
import VisiblePostList from './containers/VisiblePostList';
import DetailedPost from './containers/DetailedPost';

//setup redux store
let store = createStore(reducer, applyMiddleware(thunkMiddleware));

//render the application
ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route component={App}>
                <Route path="/" component={VisiblePostList}/>
                <Route path="/login" component={Login}/>
                <Route path="/post" component={DetailedPost}/>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('root')
)