import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class NavLink extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <label className="NavLink" onClick={this.props.onClick}>{this.props.label}</label>
        );
    }
}

NavLink.propTypes = {
    onClick: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired
};

export default NavLink;