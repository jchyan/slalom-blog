import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Post extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr>
                <td className="postTitle" onClick={this.props.onClick}>{this.props.title}</td>
                <td>{this.props.user}</td>
                <td>{new Date(this.props.datePosted).toString()}</td>
                <td>{new Date(this.props.dateUpdated).toString()}</td>
            </tr>
        );
    }
}

Post.propTypes = {
    onClick: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string,
    user: PropTypes.string.isRequired,
    datePosted: PropTypes.number.isRequired,
    dateUpdated: PropTypes.number.isRequired
};

export default Post;