import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { startNewPost } from '../actions';
import NavLink from '../components/NavLink';

export class Nav extends Component {
    constructor(props) {
        super(props);
        this.handleCreatePost = this.handleCreatePost.bind(this);
        this.handleHome = this.handleHome.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    //handle navigating to Create Post
    handleCreatePost(e) {
        e.preventDefault();
        this.props.dispatch(startNewPost());
        browserHistory.push('/post');
    }

    //handle navigating to home (aka the post list right now)
    handleHome(e) {
        e.preventDefault();
        browserHistory.push('/');
    }

    //handle navigating to login
    handleLogin(e) {
        e.preventDefault();
        browserHistory.push('/login');
    }

    render() {
        let postLink = null;

        //only show Create Post link if user is logged in
        if (this.props.currentUser) {
            postLink = (<NavLink label="Create Post" onClick={this.handleCreatePost}/>);
        }

        return (
            <div>
                <NavLink label="Home" onClick={this.handleHome}/>
                <NavLink label="Login" onClick={this.handleLogin}/>
                {postLink}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser
    }
};

export default connect(mapStateToProps)(Nav);