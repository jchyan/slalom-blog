import React, {Component} from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { addPost, fetchPosts, deletePost, updatePost, changeReadOnly } from '../actions';

export class DetailedPost extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    //handle either saving a new post or saving an existing post
    handleSubmit(e) {
        e.preventDefault();

        const isNewPost = (typeof this.props.selectedPost.id === 'undefined')
        if (isNewPost) {
            var newPost = {
                title: this.titleInput.value,
                content: this.contentInput.value,
                user: this.props.currentUser,
                datePosted: Date.now(),
                dateUpdated: Date.now()
            }
            this.props.dispatch(addPost(newPost)).then(() => {
                this.props.dispatch(fetchPosts()).then(() => {
                    browserHistory.push('/');
                });
            });
        }
        else {
            var updatedPost = {
                title: this.titleInput.value,
                content: this.contentInput.value,
                user: this.props.currentUser,
                datePosted: this.props.selectedPost.datePosted,
                dateUpdated: Date.now()
            }
            var id = this.props.selectedPost.id;
            this.props.dispatch(updatePost(id, updatedPost)).then(() => {
                this.props.dispatch(fetchPosts()).then(() => {
                    this.props.dispatch(changeReadOnly(true));
                });
            });
        }
    }

    //handle deleting a post
    handleDelete(e) {
        e.preventDefault();
        this.props.dispatch(deletePost(this.props.selectedPost)).then(() => {
            this.props.dispatch(fetchPosts()).then(() => {
                browserHistory.push('/');
            });
        });
    }

    //handle changing to edit mode
    handleEditClick(e) {
        e.preventDefault();
        this.props.dispatch(changeReadOnly(false));
    }

    //handle cancel editing
    handleCancel(e) {
        e.preventDefault();
        this.props.dispatch(fetchPosts()).then(() => {
            browserHistory.push('/');
        });
    }

    //determine default value based on whether we have a selected post
    getTitleDefaultValue() {
        if (typeof this.props.selectedPost === 'undefined') {
            return '';
        }

        if (typeof this.props.selectedPost.title === 'undefined') {
            return '';
        }
        else {
            return this.props.selectedPost.title;
        }
    }

    //determine default value based on whether we have a selected post
    getContentDefaultValue() {
        if (typeof this.props.selectedPost === 'undefined') {
            return '';
        }

        if (typeof this.props.selectedPost.content === 'undefined') {
            return '';
        }
        else {
            return this.props.selectedPost.content;
        }
    }

    render() {
        const isReadOnly = this.props.isReadOnly;
        let saveButton = null;
        let fieldsetDisabled = true;
        let buttons = null;

        //setup what user sees based on readonly status
        if (isReadOnly) {
            if (this.props.selectedPost.user === this.props.currentUser) {
                buttons = (
                    <p>
                        <button type="button" onClick={this.handleEditClick}>Edit</button>
                        <button type="button" onClick={this.handleDelete}>Delete</button>
                    </p>
                );
            }
        }
        else {
            fieldsetDisabled = false;
            buttons = (
                <p>
                    <button type="submit">Save</button>
                    <button type="button" onClick={this.handleCancel}>Cancel</button>
                </p>
            );
        }

        //determine title and content defaultValues
        let titleValue = this.getTitleDefaultValue();
        let contentValue = this.getContentDefaultValue();

        /*
        I want to set the initial value of the title and content input elements. But,
        the initial value is different based on whether I'm creating a post or clicking on
        an existing post. I don't want to have to handle every change to the input value.
        I'm using uncontrolled component for now for my input elements with defaultValue prop.
        However, when react re-renders the form, the defaultValue is not updated. In order to
        workaround this, I added a key prop and set it to the same thing as defaultValue. Now,
        when I change the defaultValue, the key also changes and the new defaultValue will be
        re-rendered.
        */
        return (
            <div>
                <form id="postForm" onSubmit={this.handleSubmit}>
                    <fieldset disabled={fieldsetDisabled}>
                        <p>
                            <input
                                placeholder="Title"
                                defaultValue={titleValue}
                                key={titleValue}
                                ref={node => this.titleInput = node}
                            />
                        </p>
                        <p>
                            <textarea
                                form="postForm"
                                placeholder="Enter content here"
                                defaultValue={contentValue}
                                key={contentValue}
                                ref={node => this.contentInput = node}
                            ></textarea>
                        </p>
                    </fieldset>
                    {buttons}
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser,
        selectedPost: state.selectedPost,
        isReadOnly: state.isReadOnly
    }
};

export default connect(mapStateToProps)(DetailedPost);