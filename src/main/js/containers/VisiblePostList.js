import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { viewPost, fetchPosts } from '../actions';
import Post from '../components/Post';
import Search from './Search';

export class VisiblePostList extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(fetchPosts());
    }

    render() {
        return (
            <div>
                <Search/>
                <table>
                    <tbody>
                        <tr>
                            <th>Title</th>
                            <th>User</th>
                            <th>Date Posted</th>
                            <th>Date Updated</th>
                        </tr>
                        {this.props.posts.map(post =>
                            <Post key={post.id} {...post} onClick={() => {
                                this.props.dispatch(viewPost(post));
                                browserHistory.push('/post');
                            }}/>
                        )}
                    </tbody>
                </table>
            </div>
        );
    }
}

VisiblePostList.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired,
        datePosted: PropTypes.number.isRequired,
        dateUpdated: PropTypes.number.isRequired
    }).isRequired).isRequired,
};

const mapStateToProps = (state) => {
    return {
        posts: state.posts
    }
};

export default connect(mapStateToProps)(VisiblePostList);