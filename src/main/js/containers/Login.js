import React, {Component} from 'react';
import { connect } from 'react-redux';
import { logIn } from '../actions';

export class Login extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    //handle user logging in
    handleSubmit(e) {
        e.preventDefault();
        if (!this.usernameInput.value) {
            alert('Please enter a username to log in');
        }
        else {
            this.props.dispatch(logIn(this.usernameInput.value));
        }
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <input placeholder="Username" ref={node => this.usernameInput = node}/>
                    <button type="submit">Login</button>
                </form>
                Logged in as {this.props.currentUser}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser
    }
};

export default connect(mapStateToProps)(Login);