import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { fetchPosts } from '../actions';

export class Search extends Component {
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
    }

    //handle search
    handleSearch(e) {
        e.preventDefault();

        if (!this.titleCheckbox.checked && !this.userCheckbox.checked && this.searchInput.value) {
            alert('Select at least one field to search by');
            return;
        }

        //make array of search parameters based on checkboxes
        let searchParams = [{
            value: this.titleCheckbox.value,
            isChecked: this.titleCheckbox.checked
        },
        {
            value: this.userCheckbox.value,
            isChecked: this.userCheckbox.checked
        }]

        this.props.dispatch(fetchPosts(this.getUrlQuery(this.searchInput.value, searchParams)));
    }

    /*
    Description: converts the search params into url query string
    Parameters:
            input - user input search string
            searchParams - array of search parameter objects; example search parameter object
                {
                    value: 'this is the parameter value'
                    isChecked: <true,false>
                }
     */
    getUrlQuery(input, searchParams) {
        let urlQuery = ''
        if (input) {
            urlQuery += '?';
            for (let sp of searchParams) {
                if (sp.isChecked) {
                    urlQuery += (sp.value + '=' + this.replaceSpaces(input) + '&');
                }
            }
            urlQuery = urlQuery.slice(0,-1);
        }
        return urlQuery;
    }

    /*
    Description: replaces spaces in search string with %20 so we can use it in url query string
    Parameters:
            input - user input search string
    */
    replaceSpaces(input) {
        return input.replace(/ /g,"%20");
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSearch}>
                    <input type="search" placeholder="Search" ref={node => this.searchInput = node}/>
                    <button type="submit">Search</button>
                    <fieldset>
                        <legend>Search By</legend>
                        <input
                            type="checkbox"
                            id="title"
                            name="filter"
                            value="title"
                            ref={node => this.titleCheckbox = node}
                        />
                        <label htmlFor="title">Title</label>
                        <input
                            type="checkbox"
                            id="user"
                            name="filter"
                            value="user"
                            ref={node => this.userCheckbox = node}
                        />
                        <label htmlFor="user">User</label>
                    </fieldset>
                </form>
            </div>
        );
    }
}

export default connect()(Search);