import {
    REQUEST_FETCH_POSTS,
    RECEIVE_FETCH_POSTS,
    REQUEST_ADD_POST,
    RECEIVE_ADD_POST,
    REQUEST_DELETE_POST,
    RECEIVE_DELETE_POST,
    REQUEST_UPDATE_POST,
    RECEIVE_UPDATE_POST,
    LOG_IN,
    VIEW_POST,
    START_NEW_POST,
    CHANGE_READ_ONLY
} from './actions';

const initialState = {
    posts: [],                      //array of posts we fetched from the database
    isFetchingPosts: false,         //indicates still waiting on response from server for http GET
    isAddingPost: false,            //indicates still waiting on response from server for http POST
    isDeletingPost: false,          //indicates still waiting on response from server for http DELETE
    isUpdatingPost: false,          //indicates still waiting on response from server for http PUT
    currentUser: null,              //the current user that's logged in
    selectedPost: {},               //the selected post for detailed post view
    isReadOnly: true                //indicates whether the detailed post view is readonly
}

export default (state = initialState, action) => {
    switch (action.type) {
        case REQUEST_FETCH_POSTS:
            return Object.assign({}, state, {
                isFetchingPosts: true
            });
        case RECEIVE_FETCH_POSTS:
            return Object.assign({}, state, {
                posts: action.data,
                isFetchingPosts: false
            });
        case REQUEST_ADD_POST:
            return Object.assign({}, state, {
                isAddingPost: true
            });
        case RECEIVE_ADD_POST:
            return Object.assign({}, state, {
                isAddingPost: false
            });
        case REQUEST_DELETE_POST:
            return Object.assign({}, state, {
                isDeletingPost: true
            });
        case RECEIVE_DELETE_POST:
            return Object.assign({}, state, {
                isDeletingPost: false
            });
        case REQUEST_UPDATE_POST:
            return Object.assign({}, state, {
                isUpdatingPost: true
            });
        case RECEIVE_UPDATE_POST:
            return Object.assign({}, state, {
                isUpdatingPost: false
            });
        case LOG_IN:
            return Object.assign({}, state, {
                currentUser: action.username
            });
        case VIEW_POST:
            return Object.assign({}, state, {
                selectedPost: action.post,
                isReadOnly: true
            });
        case START_NEW_POST:
            return Object.assign({}, state, {
                selectedPost: {},
                isReadOnly: false
            });
        case CHANGE_READ_ONLY:
            return Object.assign({}, state, {
                isReadOnly: action.status
            });
        default:
            return state;
    }
};