export const REQUEST_FETCH_POSTS = 'REQUEST_FETCH_POSTS';       //sent http GET request to server
export const RECEIVE_FETCH_POSTS = 'RECEIVE_FETCH_POSTS';       //received http GET response from server
export const REQUEST_ADD_POST = 'REQUEST_ADD_POST';             //sent http POST request to server
export const RECEIVE_ADD_POST = 'RECEIVE_ADD_POST';             //received http POST response from server
export const REQUEST_DELETE_POST = 'REQUEST_DELETE_POST';       //sent http DELETE request to server
export const RECEIVE_DELETE_POST = 'RECEIVE_DELETE_POST';       //received http DELETE response from server
export const REQUEST_UPDATE_POST = 'REQUEST_UPDATE_POST';       //sent http PUT request to server
export const RECEIVE_UPDATE_POST = 'RECEIVE_UPDATE_POST';       //received http PUT response from server
export const LOG_IN = 'LOG_IN';                                 //user logs in with a username
export const VIEW_POST = 'VIEW_POST';                           //user clicked on a post to view in more detail
export const START_NEW_POST = 'START_NEW_POST';                 //user is creating a new post
export const CHANGE_READ_ONLY = 'CHANGE_READ_ONlY';             //change the readonly status of the detailed post view

function requestFetchPosts() {
    return {
        type: REQUEST_FETCH_POSTS
    }
}
/*
Parameters:
        data - array of post objects
 */
function receiveFetchPosts(data) {
    return {
        type: RECEIVE_FETCH_POSTS,
        data: data
    }
}

/*
Parameters:
        urlQuery - the url for search queries; by default this is an empty string for when we want to fetch all posts
 */
export function fetchPosts(urlQuery = '') {
    let url = '/api/posts/get'

    return (dispatch, getState) => {
        dispatch(requestFetchPosts());
        return fetch(url + urlQuery, {
            method: 'GET'
        }).then(response => {
            return response.json();
        }).then(data => {
            dispatch(receiveFetchPosts(data));
        });
    }
}

function requestAddPost() {
    return {
        type: REQUEST_ADD_POST
    }
}

function receiveAddPost() {
    return {
        type: RECEIVE_ADD_POST,
    }
}

/*
Parameters:
        newPost - post object that we want to save to server
 */
export function addPost(newPost) {
    return (dispatch) => {
        dispatch(requestAddPost());
        return fetch('/api/posts', {
            method: 'POST',
            body: JSON.stringify(newPost),
            headers: {'Content-Type': 'application/json'}
        }).then(response => {
            return response.json();
        }).then(data => {
            dispatch(receiveAddPost());
        });
    }
}

function requestDeletePost() {
    return {
        type: REQUEST_DELETE_POST
    }
}

function receiveDeletePost() {
    return {
        type: RECEIVE_DELETE_POST
    }
}

/*
Parameters:
        post - post object that we want to delete from server
 */
export function deletePost(post) {
    return (dispatch) => {
        dispatch(requestDeletePost());
        return fetch('/api/posts/'+post.id, {
            method: 'DELETE',
            body: JSON.stringify(post),
            headers: {'Content-Type': 'application/json'}
        }).then(response => {
            dispatch(receiveDeletePost());
        });
    }
}

function requestUpdatePost() {
    return {
        type: REQUEST_UPDATE_POST
    }
}

function receiveUpdatePost() {
    return {
        type: RECEIVE_UPDATE_POST
    }
}

/*
Parameters:
        id - id of the post we want to update
        post - post object data that we want to save to server
 */
export function updatePost(id, post) {
    return (dispatch) => {
        dispatch(requestUpdatePost());
        return fetch('/api/posts/'+id, {
            method: 'PUT',
            body: JSON.stringify(post),
            headers: {'Content-Type': 'application/json'}
        }).then(response => {
            dispatch(receiveUpdatePost());
        });
    }
}

/*
Parameters:
        username - username the user has input to log in with
 */
export function logIn(username) {
    return {
        type: LOG_IN,
        username: username
    }
}

/*
Parameters:
        post - post object the user wants to view
 */
export function viewPost(post) {
    return {
        type: VIEW_POST,
        post: post
    }
}

export function startNewPost() {
    return {
        type: START_NEW_POST
    }
}

/*
Parameters:
        status - boolean for what we want to change the readonly status to
 */
export function changeReadOnly(status) {
    return {
        type: CHANGE_READ_ONLY,
        status: status
    }
}