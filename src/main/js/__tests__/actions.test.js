import * as actions from '../actions';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetch from 'isomorphic-fetch';
import fetchMock from 'fetch-mock';
//import nock from 'nock';

const middlewares = [ thunk ];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
    describe('fetchPosts', () => {

        afterEach(() => {
            //nock.cleanAll();
            fetchMock.restore();
        });

        it('creates RECEIVE_FETCH_POSTS when fetching posts has been done', () => {
            const testData = [
                                 {
                                     id : 'test id',
                                     title: 'test title',
                                     content: 'test content',
                                     user: 'test user',
                                     datePosted: 999,
                                     dateUpdated: 999
                                 }
                             ]

            //nock('http://localhost:0000').get('/api/posts/get').reply(200, testData);

            const expectedActions = [
                {
                    type: actions.REQUEST_FETCH_POSTS
                },
                {
                    type: actions.RECEIVE_FETCH_POSTS,
                    data: testData
                }
            ]

            const store = mockStore({posts: []});

            fetchMock.get('/api/posts/get', testData);

            return store.dispatch(actions.fetchPosts()).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });

    describe('addPost', () => {
        afterEach(() => {
            //nock.cleanAll();
            fetchMock.restore();
        });

        it('creates RECEIVE_ADD_POST when adding post is done', () => {
            const testData = [
                                 {
                                     title: 'test title',
                                     content: 'test content',
                                     user: 'test user',
                                     datePosted: 999,
                                     dateUpdated: 999
                                 }
                             ]

            //nock('http://localhost:0000').get('/api/posts/get').reply(200, testData);

            const expectedActions = [
                {
                    type: actions.REQUEST_ADD_POST
                },
                {
                    type: actions.RECEIVE_ADD_POST
                }
            ]

            const store = mockStore({posts: []});

            fetchMock.post('/api/posts', testData);

            return store.dispatch(actions.addPost(testData)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });

    describe('deletePost', () => {
        afterEach(() => {
            //nock.cleanAll();
            fetchMock.restore();
        });

        it('creates RECEIVE_DELETE_POST when deleting post is done', () => {
            const testData = {
                                id: 'testid',
                                title: 'test title',
                                content: 'test content',
                                user: 'test user',
                                datePosted: 999,
                                dateUpdated: 999
                             }

            //nock('http://localhost:0000').get('/api/posts/get').reply(200, testData);

            const expectedActions = [
                {
                    type: actions.REQUEST_DELETE_POST
                },
                {
                    type: actions.RECEIVE_DELETE_POST
                }
            ]

            const store = mockStore({posts: []});

            fetchMock.delete('/api/posts/'+testData.id, testData);

            return store.dispatch(actions.deletePost(testData)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });

    describe('updatePost', () => {
        afterEach(() => {
            //nock.cleanAll();
            fetchMock.restore();
        });

        it('creates RECEIVE_UPDATE_POST when updating post is done', () => {
            const testData = {
                                title: 'test title',
                                content: 'test content',
                                user: 'test user',
                                datePosted: 999,
                                dateUpdated: 999
                             }
            const testId = 'testid'

            //nock('http://localhost:0000').get('/api/posts/get').reply(200, testData);

            const expectedActions = [
                {
                    type: actions.REQUEST_UPDATE_POST
                },
                {
                    type: actions.RECEIVE_UPDATE_POST
                }
            ]

            const store = mockStore({posts: []});

            fetchMock.put('/api/posts/'+testId, testData);

            return store.dispatch(actions.updatePost(testId, testData)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });
});

describe('actions', () => {
    describe(actions.LOG_IN, () => {
        it('should create an action to log in with a username', () => {
            const testUser = 'testuser'
            const expectedAction = {
                type: actions.LOG_IN,
                username: testUser
            }
            expect(actions.logIn(testUser)).toEqual(expectedAction);
        });
    });
    describe(actions.VIEW_POST, () => {
        it('should create an action to view a post', () => {
            const testPost = {
                id : 'test id',
                title: 'test title',
                content: 'test content',
                user: 'test user',
                datePosted: 999,
                dateUpdated: 999
            };
            const expectedAction = {
                type: actions.VIEW_POST,
                post: testPost
            }
            expect(actions.viewPost(testPost)).toEqual(expectedAction);
        });
    });
    describe(actions.START_NEW_POST, () => {
        it('should create an action to start a new post', () => {
            const expectedAction = {
                type: actions.START_NEW_POST
            };
            expect(actions.startNewPost()).toEqual(expectedAction);
        });
    });
    describe(actions.CHANGE_READ_ONLY, () => {
        it('should create an action to change readonly status for detailed post UI', () => {
            const testStatus = true;
            const expectedAction = {
                type: actions.CHANGE_READ_ONLY,
                status: testStatus
            };
            expect(actions.changeReadOnly(testStatus)).toEqual(expectedAction);
        });
    });
});