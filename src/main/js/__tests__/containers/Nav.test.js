import React from 'react';
import { shallow, mount, render } from 'enzyme';
import ConnectedNav, { Nav } from '../../containers/Nav';
import NavLink from '../../components/NavLink';

describe('Nav', () => {
    it('should render', () => {
        const enzymeWrapper = shallow(<Nav currentUser='testuser'/>);

        expect(enzymeWrapper.find('div').length).toBe(1);

        expect(enzymeWrapper.find(NavLink).length).toBe(3);

        const homeNavLinkProps = enzymeWrapper.find(NavLink).at(0).props();
        expect(homeNavLinkProps.onClick).toBeDefined();

        const loginNavLinkProps = enzymeWrapper.find(NavLink).at(1).props();
        expect(loginNavLinkProps.onClick).toBeDefined();

        const createPostNavLinkProps = enzymeWrapper.find(NavLink).at(2).props();
        expect(createPostNavLinkProps.onClick).toBeDefined();
    });

    /* couldn't test, got SecurityError
    it('should handle clicking on Home', () => {
        const spy = jest.fn()
        const enzymeWrapper = shallow(<Nav dispatch={spy} currentUser='testuser'/>);

        const link = enzymeWrapper.find(NavLink).at(0);

        link.simulate('click', {preventDefault() {}});
        expect(spy).toHaveBeenCalled();

        spy.mockReset();
        spy.mockRestore();
    });
    */
});