import React from 'react';
import { shallow, mount, render } from 'enzyme';
import ConnectedLogin, { Login } from '../../containers/Login';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import reducer from '../../reducers';
import { createStore, applyMiddleware } from 'redux';

function setupLogin() {

    const enzymeWrapper = shallow(<Login/>);

    return {
        enzymeWrapper
    }
}

/*
function setupConnectedLogin() {
    const props = {
        currentUser:
    };
}
*/

describe('Login', () => {
    it('should render', () => {
        const { enzymeWrapper } = setupLogin();

        expect(enzymeWrapper.find('div').length).toBe(1);

        const formProps = enzymeWrapper.find('form').props();
        expect(formProps.onSubmit).toBeDefined();

        const inputProps = enzymeWrapper.find('input').props();
        expect(inputProps.placeholder).toBe('Username');

        const buttonProps = enzymeWrapper.find('button').props();
        expect(buttonProps.type).toBe('submit');
    });

    it('should handle submit', () => {
        const spy = jest.fn()
        const enzymeWrapper = shallow(<Login dispatch={spy}/>);

        const form = enzymeWrapper.find('form');
        enzymeWrapper.instance().usernameInput = enzymeWrapper.find('input');
        enzymeWrapper.instance().usernameInput.value = '';

        form.simulate('submit', {preventDefault() {}});
        expect(spy).not.toHaveBeenCalled();

        enzymeWrapper.instance().usernameInput = enzymeWrapper.find('input');
        enzymeWrapper.instance().usernameInput.value = 'my input';

        form.simulate('submit', {preventDefault() {}});
        expect(spy).toHaveBeenCalled();

        spy.mockReset();
        spy.mockRestore();
    });

});