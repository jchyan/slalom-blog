import React from 'react';
import { shallow, mount, render } from 'enzyme';
import ConnectedSearch, { Search } from '../../containers/Search';

describe('Search', () => {
    it('should render', () => {
        const enzymeWrapper = shallow(<Search/>);

        expect(enzymeWrapper.find('div').length).toBe(1);
        expect(enzymeWrapper.find('form').length).toBe(1);
        expect(enzymeWrapper.find('input').length).toBe(3);
        expect(enzymeWrapper.find('button').length).toBe(1);
        expect(enzymeWrapper.find('fieldset').length).toBe(1);
        expect(enzymeWrapper.find('legend').length).toBe(1);
        expect(enzymeWrapper.find('label').length).toBe(2);
    });

    test('replaceSpaces', () => {
        const enzymeWrapper = shallow(<Search/>);

        const input = 'this is a test';

        expect(enzymeWrapper.instance().replaceSpaces(input)).toBe('this%20is%20a%20test');
    });

    test('getUrlQuery', () => {
        const enzymeWrapper = shallow(<Search/>);

        const input = 'testinput';
        const searchParams = [{
            value: 'value1',
            isChecked: true
        },
        {
            value: 'value2',
            isChecked: false
        },
        {
            value: 'value3',
            isChecked: true
        }];

        expect(enzymeWrapper.instance().getUrlQuery(input,searchParams)).toBe('?value1=testinput&value3=testinput');
    });

    it('should handle submit', () => {
        const spy = jest.fn()
        const enzymeWrapper = shallow(<Search dispatch={spy}/>);

        const form = enzymeWrapper.find('form');

        enzymeWrapper.instance().searchInput = enzymeWrapper.find('input').at(0);
        enzymeWrapper.instance().searchInput.value = 'test search';

        enzymeWrapper.instance().titleCheckbox = enzymeWrapper.find('input').at(1);
        enzymeWrapper.instance().titleCheckbox.value = 'title';
        enzymeWrapper.instance().titleCheckbox.checked = false;

        enzymeWrapper.instance().userCheckbox = enzymeWrapper.find('input').at(2);
        enzymeWrapper.instance().userCheckbox.value = 'user';
        enzymeWrapper.instance().userCheckbox.checked = false;

        form.simulate('submit', {preventDefault() {}});
        expect(spy).not.toHaveBeenCalled();

        enzymeWrapper.instance().titleCheckbox = enzymeWrapper.find('input').at(1);
        enzymeWrapper.instance().titleCheckbox.value = 'title';
        enzymeWrapper.instance().titleCheckbox.checked = true;

        enzymeWrapper.instance().userCheckbox = enzymeWrapper.find('input').at(2);
        enzymeWrapper.instance().userCheckbox.value = 'user';
        enzymeWrapper.instance().userCheckbox.checked = true;

        form.simulate('submit', {preventDefault() {}});
        expect(spy).toHaveBeenCalled();

        spy.mockReset();
        spy.mockRestore();
    });
});