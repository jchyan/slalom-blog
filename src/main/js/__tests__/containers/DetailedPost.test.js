import React from 'react';
import { shallow, mount, render } from 'enzyme';
import ConnectedDetailedPost, { DetailedPost } from '../../containers/DetailedPost';

function setupDetailedPost() {

    const enzymeWrapper = shallow(<DetailedPost/>);

    return {
        enzymeWrapper
    }
}

describe('DetailedPost', () => {
    it('should render', () => {
        const { enzymeWrapper } = setupDetailedPost();

        expect(enzymeWrapper.find('div').length).toBe(1);
        expect(enzymeWrapper.find('fieldset').length).toBe(1);
        expect(enzymeWrapper.find('p').length).toBe(3);
        expect(enzymeWrapper.find('input').length).toBe(1);
        expect(enzymeWrapper.find('textarea').length).toBe(1);

        const formProps = enzymeWrapper.find('form').props();
        expect(formProps.onSubmit).toBeDefined();
    });

    it('should getDefaultValues', () => {
        let enzymeWrapper = shallow(<DetailedPost selectedPost={{title: 'mytitle', content:'mycontent'}}/>);

        expect(enzymeWrapper.instance().getTitleDefaultValue()).toBe('mytitle');
        expect(enzymeWrapper.instance().getContentDefaultValue()).toBe('mycontent');

        enzymeWrapper = shallow(<DetailedPost selectedPost={{}}/>);
        expect(enzymeWrapper.instance().getTitleDefaultValue()).toBe('');
        expect(enzymeWrapper.instance().getContentDefaultValue()).toBe('');
    })

    it('should handle clicking on edit', () => {
        const spy = jest.fn()
        const post = {
            user: 'testuser'
        }
        const enzymeWrapper = shallow(<DetailedPost dispatch={spy} selectedPost={post} currentUser='testuser' isReadOnly={true}/>);

        const button = enzymeWrapper.find('button').at(0);

        button.simulate('click', {preventDefault() {}});
        expect(spy).toHaveBeenCalled();

        spy.mockReset();
        spy.mockRestore();
    })

});