import React from 'react';
import { shallow, mount, render } from 'enzyme';
import ConnectedVisiblePostList, { VisiblePostList } from '../../containers/VisiblePostList';
import Search from '../../containers/Search';
import Post from '../../components/Post';

const posts = [
    {
        id: '1',
        title: 'post1',
        content: 'content1',
        user: 'user1',
        datePosted: 999,
        dateUpdated: 999
    },
    {
        id: '2',
        title: 'post2',
        content: 'content2',
        user: 'user2',
        datePosted: 999,
        dateUpdated: 999
    }
]

describe('VisiblePostList', () => {
    it('should render', () => {
        const enzymeWrapper = shallow(<VisiblePostList posts={posts}/>);

        expect(enzymeWrapper.find('div').length).toBe(1);
        expect(enzymeWrapper.find(Search).length).toBe(1);
        expect(enzymeWrapper.find(Post).length).toBe(posts.length);
        expect(enzymeWrapper.find('table').length).toBe(1);
        expect(enzymeWrapper.find('tbody').length).toBe(1);
        expect(enzymeWrapper.find('tr').length).toBe(1);
        expect(enzymeWrapper.find('th').length).toBe(4);
    });
});