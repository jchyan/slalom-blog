import reducer from '../reducers'
import * as actions from '../actions';

describe('reducers', () => {
    const initialState = {
        posts: [],
        isFetchingPosts: false,
        isAddingPost: false,
        isDeletingPost: false,
        isUpdatingPost: false,
        currentUser: null,
        selectedPost: {},
        isReadOnly: true
    }

    it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState)
    })

    it('should handle REQUEST_FETCH_POSTS', () => {
        expect(reducer(initialState, {
            type: actions.REQUEST_FETCH_POSTS
        })).toEqual({
            posts: [],
            isFetchingPosts: true,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        })
    })

    it('should handle handle RECEIVE_FETCH_POSTS', () => {
        const testData = [{
            id : 'test id',
            title: 'test title',
            content: 'test content',
            user: 'test user',
            datePosted: 999,
            dateUpdated: 999
        }]
        expect(reducer({
            posts: [],
            isFetchingPosts: true,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        }, {
            type: actions.RECEIVE_FETCH_POSTS,
            data: testData
        })).toEqual({
            posts: testData,
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        })
    })

    it('should handle REQUEST_ADD_POST', () => {
        expect(reducer(initialState, {
            type: actions.REQUEST_ADD_POST
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: true,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        })
    })

    it('should handle RECEIVE_ADD_POST', () => {
        expect(reducer({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: true,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        }, {
            type: actions.RECEIVE_ADD_POST
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        })
    })

    it('should handle REQUEST_DELETE_POST', () => {
        expect(reducer(initialState, {
            type: actions.REQUEST_DELETE_POST
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: true,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        })
    })

    it('should handle RECEIVE_DELETE_POST', () => {
        expect(reducer({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: true,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        }, {
            type: actions.RECEIVE_DELETE_POST
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        })
    })

    it('should handle REQUEST_UPDATE_POST', () => {
        expect(reducer(initialState, {
            type: actions.REQUEST_UPDATE_POST
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: true,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        })
    })

    it('should handle RECEIVE_UPDATE_POST', () => {
        expect(reducer({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: true,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        }, {
            type: actions.RECEIVE_UPDATE_POST
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: true
        })
    })

    it('should handle LOG_IN', () => {
        const testUser = 'testUser'
        expect(reducer(initialState, {
            type: actions.LOG_IN,
            username: testUser
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: testUser,
            selectedPost: {},
            isReadOnly: true
        })
    })

    it('should handle VIEW_POST', () => {
        const testPost = {
            id : 'test id',
            title: 'test title',
            content: 'test content',
            user: 'test user',
            datePosted: 999,
            dateUpdated: 999
        }
        expect(reducer({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: false
        }, {
            type: actions.VIEW_POST,
            post: testPost
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: testPost,
            isReadOnly: true
        })
    })

    it('should handle START_NEW_POST', () => {
        expect(reducer({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {data: 'dummy data'},
            isReadOnly: true
        }, {
            type: actions.START_NEW_POST
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: false
        })
    })

    it('should handle CHANGE_READ_ONLY', () => {
        expect(reducer(initialState, {
            type: actions.CHANGE_READ_ONLY,
            status: false
        })).toEqual({
            posts: [],
            isFetchingPosts: false,
            isAddingPost: false,
            isDeletingPost: false,
            isUpdatingPost: false,
            currentUser: null,
            selectedPost: {},
            isReadOnly: false
        })
    })
})