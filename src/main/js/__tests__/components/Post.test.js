import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Post from '../../components/Post';

function setup() {
    const props = {
        onClick: jest.fn(),
        title: 'test title',
        content: 'test content',
        user: 'test user',
        datePosted: 999,
        dateUpdated: 999
    };

    const enzymeWrapper = shallow(<Post {...props}/>);

    return {
        props,
        enzymeWrapper
    }
}

describe('Post', () => {
    it('should render', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('tr').exists()).toBe(true);
        expect(enzymeWrapper.find('td').length).toBe(4);

        const titleTdProps = enzymeWrapper.find('td').at(0).props();
        expect(titleTdProps.onClick).toBeDefined();
    });

    it('should call onClick prop when clicking on title td', () => {
        const { props, enzymeWrapper} = setup();
        const titleTd = enzymeWrapper.find('td').at(0);
        titleTd.props().onClick();
        expect(props.onClick.mock.calls.length).toBe(1);
    });
});