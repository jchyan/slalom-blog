import React from 'react';
import { shallow, mount, render } from 'enzyme';
import NavLink from '../../components/NavLink';

function setup() {
    const props = {
        onClick: jest.fn(),
        label: 'test label'
    };

    const enzymeWrapper = shallow(<NavLink {...props}/>);

    return {
        props,
        enzymeWrapper
    }
}

describe('NavLink', () => {
    it('should render', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('label').length).toBe(1);

        const titleTdProps = enzymeWrapper.find('label').props();
        expect(titleTdProps.onClick).toBeDefined();
    });

    it('should call onClick prop when clicking on label', () => {
        const { props, enzymeWrapper} = setup();
        const label = enzymeWrapper.find('label');
        label.props().onClick();
        expect(props.onClick.mock.calls.length).toBe(1);
    });
});