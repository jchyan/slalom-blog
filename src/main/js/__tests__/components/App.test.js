import React from 'react';
import { shallow, mount, render } from 'enzyme';
import App from '../../components/App';
import Nav from '../../containers/Nav';

function setup() {
    const enzymeWrapper = shallow(<App/>);

    return {
        enzymeWrapper
    }
}

describe('App', () => {
    it('should render', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('div').exists()).toBe(true);
        expect(enzymeWrapper.find(Nav).exists()).toBe(true);

    });
});